![Header](https://gitlab.com/ilonab/ilonab/blob/ad918d30b1424e1ec3a407f5f6f84e431a5ab958/assets/qa-2-min.png)

## I'm a QA Engineer

## My CV 
[Link to my CV](https://docs.google.com/document/d/13SuoJFGTi75o5ybjAaybgg-OAlNUddUoOg635ZNnTFk/edit)

### Languages and Tolls

![Postman](https://img.shields.io/badge/-Postman-F37F40??style=for-the-badge&logo=postman&logoColor=FFFFFF)
![Figma](https://img.shields.io/badge/-Figma-9C9C9C?style=for-the-badge&logo=figma)
![Blisk](https://img.shields.io/badge/-Blisk-413A73?style=for-the-badge&logo=blisk)
![Fiddler](https://img.shields.io/badge/-Fiddler-53A750?style=for-the-badge&logo=fiddler)
![Charles](https://img.shields.io/badge/-Charles-C0E0F1?style=for-the-badge&logo=charles)
![Git](https://img.shields.io/badge/-Git-E84D31?style=for-the-badge&logo=git&logoColor=F7F7F7)
![GitLab](https://img.shields.io/badge/-GitLab-999999?style=for-the-badge&logo=GitLab)
![Jira](https://img.shields.io/badge/-Jira-2580F7?style=for-the-badge&logo=jira)
![MySQL](https://img.shields.io/badge/-MySQL-DDD7D9?style=for-the-badge&logo=mysql)
![Jenkins](https://img.shields.io/badge/-Jenkins-E1E1E1?style=for-the-badge&logo=jenkins)
![TestRail](https://img.shields.io/badge/-TestRail-0B3551?style=for-the-badge&logo=testrail)
![TS Capture](https://img.shields.io/badge/-TSCapture-F4BC02?style=for-the-badge&logo=capture)
![DevTools](https://img.shields.io/badge/-DevTools-008FFC?style=for-the-badge&logo=DevTools)

### Follow me
[![LinkedIn](https://img.shields.io/badge/-LinkedIn-2764AC??style=for-the-badge&logo=linkedin&logoColor=FFFFFF)](https://www.linkedin.com/in/ilona-borysova-36556a246/)
[![Telegram](https://img.shields.io/badge/-Telegram-B2F5F7?style=for-the-badge&logo=Telegram)](https://t.me/IlonaBorysova)
[![Gmail](https://img.shields.io/badge/-Gmail-F2B500?style=for-the-badge&logo=Gmail)](ilona.borysova@gmail.com)
[![Skype](https://img.shields.io/badge/-Skype-53CBF7??style=for-the-badge&logo=skype&logoColor=FFFFFF)](https://join.skype.com/invite/EGbXBCzfaxVS)